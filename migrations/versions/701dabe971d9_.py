"""RapiD and JOSM MapWithAI defaults (4 -> 10005)

Revision ID: 701dabe971d9
Revises: 2ee4bca188a9
Create Date: 2021-05-06 15:18:46.149174

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "701dabe971d9"
down_revision = "51ccc46f1b8a"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        "projects",
        "mapping_editors",
        type_=sa.ARRAY(sa.Integer),
        postgresql_using="array_replace(validation_editors, 4, 10005);",
    )

    op.alter_column(
        "projects",
        "validation_editors",
        type_=sa.ARRAY(sa.Integer),
        postgresql_using="array_replace(validation_editors, 4, 10005);",
    )
    pass


def downgrade():
    op.alter_column(
        "projects",
        "mapping_editors",
        type_=sa.ARRAY(sa.Integer),
        postgresql_using="array_replace(validation_editors, 4, 10005);",
    )

    op.alter_column(
        "projects",
        "validation_editors",
        type_=sa.ARRAY(sa.Integer),
        postgresql_using="array_replace(validation_editors, 4, 10005);",
    )
