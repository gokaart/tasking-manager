"""Kaart customizations (see attached note if errors)

Revision ID: c24028c4e87d
Revises: 2ee4bca188a9, 7408ceebbef7, 701dabe971d9
Create Date: 2021-05-06 15:34:39.522632

"""

# revision identifiers, used by Alembic.
revision = "c24028c4e87d"
# NOTE: 7408ceebbef7 has already been applied (it was inserted into the flow, instead of branching and merging)
# When upgrading (first time), please remove it from the following array
down_revision = ("2ee4bca188a9", "7408ceebbef7", "701dabe971d9")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
