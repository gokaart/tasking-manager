import json
import datetime
import requests
from shapely.geometry import shape, mapping, LineString
from shapely.ops import linemerge
import xml.etree.ElementTree as ET
from backend.models.postgis.task import Task
import os
import math
import mapbox_vector_tile
from typing import List, Optional, Any, Generator, Tuple, Set
import geojson
from flask import current_app
from enum import IntFlag

from backend.models.postgis.utils import NotFound


class BBox:
    """ A class for storing bbox coordinates """

    def __init__(self, lat1: float, lon1: float, lat2: float, lon2: float):
        """ lat, lon, lat, lon """
        self.minLat = lat1
        self.minLon = lon1
        self.maxLat = lat2
        self.maxLon = lon2

    def __repr__(self):
        return (
            f"minLat={self.minLat}, minLon={self.minLon}, "
            + f"maxLat={self.maxLat}, maxLon={self.maxLon}"
        )

    @staticmethod
    def tile_latlon_bounds(x_coordinate: int, y_coordinate: int, zoom: int) -> "BBox":
        """Convert a tile to some rounded bounds"""
        lon1 = round(Tile.x_to_lon(x_coordinate, zoom), 6)
        lon2 = round(Tile.x_to_lon(x_coordinate + 1, zoom), 6)
        lat1 = round(Tile.y_to_lat(y_coordinate + 1, zoom), 6)
        lat2 = round(Tile.y_to_lat(y_coordinate, zoom), 6)
        return BBox(lat1, lon1, lat2, lon2)


class Bounds(IntFlag):
    """ Stores a tile's download_additional field """

    NONE = 0
    NORTH = 1
    EAST = 2
    SOUTH = 4
    WEST = 8


class Tile:
    """ A class for storing tile data """

    def __init__(
        self,
        x_coordinate: int,
        y_coordinate: int,
        zoom: int,
        extent: int = 4096,
        geom: Optional[List[dict[str, Any]]] = None,
        download_additional: IntFlag = Bounds.NONE,
        cutoff_keys: Optional[Set[Tuple[str, Bounds]]] = None,
        client_id: Optional[str] = "",
    ):
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.zoom = zoom
        self.extent = extent
        self.geometry = geom
        self.download_additional = download_additional
        self.cutoff_keys = cutoff_keys
        self.tile_bounds = BBox.tile_latlon_bounds(
            self.x_coordinate, self.y_coordinate, self.zoom
        )
        # build tile's mapillary url from properties
        # TODO update for mapillary v4
        self.url = (
            "https://tiles3.mapillary.com/v0.1/"
            + f"{self.zoom}/{self.x_coordinate}/{self.y_coordinate}.mvt"
            + (f"?client_id={client_id}" if client_id else "")
        )

    def __repr__(self) -> str:
        return f"{self.zoom}{os.path.sep}{self.x_coordinate}{os.path.sep}{self.y_coordinate}"

    def convert(self, coordinates: List[List[int]]) -> List[List[float]]:
        """Convert tile coordinates for single geom to world coordinates"""
        converted_coordinates = []
        for coords in coordinates:
            x_coor = coords[0] / self.extent
            y_coor = coords[1] / self.extent
            y_coor = (
                self.tile_bounds.minLat
                + (self.tile_bounds.maxLat - self.tile_bounds.minLat) * y_coor
            )
            x_coor = (
                self.tile_bounds.minLon
                + (self.tile_bounds.maxLon - self.tile_bounds.minLon) * x_coor
            )
            converted_coordinates.append([x_coor, y_coor])
        return converted_coordinates

    @staticmethod
    def x_to_lon(x_coordinate: int, zoom: int) -> float:
        """Convert an x/z int from a tile to a lon"""
        return (x_coordinate / 2 ** zoom) * 360.0 - 180.0

    @staticmethod
    def y_to_lat(y_coordinate: int, zoom: int) -> float:
        """Convert a y/z int from a tile to a lat"""
        temp = math.pi - 2 * math.pi * y_coordinate / 2 ** zoom
        return 180 / math.pi * math.atan((math.exp(temp) - math.exp(-temp)) / 2)

    def set_extent(self, extent: int):
        self.extent = extent

    def compute_cutoff_keys(self, tile_list):
        keys = set()
        if self.geometry:
            for f in self.geometry:
                if (
                    any(
                        coord[0] <= self.tile_bounds.minLon
                        for coord in f["geometry"]["coordinates"]
                    )
                    and (self.zoom, self.x_coordinate - 1, self.y_coordinate)
                    not in tile_list
                ):
                    keys.add((f["properties"]["key"], Bounds.WEST))
                    self.download_additional = self.download_additional | Bounds.WEST
                if (
                    any(
                        coord[0] >= self.tile_bounds.maxLon
                        for coord in f["geometry"]["coordinates"]
                    )
                    and (self.zoom, self.x_coordinate + 1, self.y_coordinate)
                    not in tile_list
                ):
                    keys.add((f["properties"]["key"], Bounds.EAST))
                    self.download_additional = self.download_additional | Bounds.EAST
                if (
                    any(
                        coord[1] <= self.tile_bounds.minLat
                        for coord in f["geometry"]["coordinates"]
                    )
                    and (self.zoom, self.x_coordinate, self.y_coordinate + 1)
                    not in tile_list
                ):
                    keys.add((f["properties"]["key"], Bounds.SOUTH))
                    self.download_additional = self.download_additional | Bounds.SOUTH
                if (
                    any(
                        coord[1] >= self.tile_bounds.maxLat
                        for coord in f["geometry"]["coordinates"]
                    )
                    and (self.zoom, self.x_coordinate, self.y_coordinate - 1)
                    not in tile_list
                ):
                    keys.add((f["properties"]["key"], Bounds.NORTH))
                    self.download_additional = self.download_additional | Bounds.NORTH
        self.cutoff_keys = keys

    def set_geometry(self, geom):
        tile_geom = []
        for f in geom:
            feature_type = f["geometry"]["type"]
            if feature_type == "MultiLineString":
                for coord in f["geometry"]["coordinates"]:
                    tile_geom.append(
                        {
                            "geometry": {
                                "type": "LineString",
                                "coordinates": coord,
                            },
                            "properties": f["properties"],
                            "id": f["id"],
                            "type": f["type"],
                        }
                    )
            elif feature_type == "LineString":
                f["type"] = "Feature"
                tile_geom.append(f)

        lat_lon_geom = []
        for geom in tile_geom:
            converted_coords = self.convert(geom["geometry"]["coordinates"])
            geom["geometry"]["coordinates"] = converted_coords
            # Tasking manager expects x/y/z properties for geometry
            geom["properties"]["x"] = self.x_coordinate
            geom["properties"]["y"] = self.y_coordinate
            geom["properties"]["zoom"] = self.zoom
            lat_lon_geom.append(geom)
        self.geometry = lat_lon_geom

    def get_tile_coordinates(self) -> Tuple[int, int, int]:
        return (self.zoom, self.x_coordinate, self.y_coordinate)

    @staticmethod
    def lat_lon_to_tile(lat: float, lon: float, zoom: int) -> "Tile":
        """Convert a lat lon to a tile at a specified zoom level"""
        tile_count = 2 ** zoom
        x_coordinate = math.floor(tile_count * (180 + lon) / 360)
        y_coordinate = math.floor(
            tile_count
            * (
                1
                - (
                    math.log(
                        math.tan(math.radians(lat)) + 1 / math.cos(math.radians(lat))
                    )
                    / math.pi
                )
            )
            / 2
        )
        return Tile(x_coordinate, y_coordinate, zoom)


def geom_in_bounds(feature, tile, bounding_box):
    """Determines if the geometry for a tile is within the bbox supplied, only needed for the inital tile fetches"""
    coordinates = feature["geometry"]["coordinates"]
    converted_coordinates = []
    if feature["geometry"]["type"] == "LineString":
        converted_coordinates = tile.convert(coordinates)
        for c in converted_coordinates:
            if (
                c[0] <= bounding_box.maxLon
                and c[0] >= bounding_box.minLon
                and c[1] <= bounding_box.maxLat
                and c[1] >= bounding_box.minLat
            ):
                return True
    elif feature["geometry"]["type"] == "MultiLineString":
        for coords in coordinates:
            converted_coordinates.append(tile.convert(coords))
        for coords in converted_coordinates:
            for c in coords:
                if (
                    c[0] <= bounding_box.maxLon
                    and c[0] >= bounding_box.minLon
                    and c[1] <= bounding_box.maxLat
                    and c[1] >= bounding_box.minLat
                ):
                    return True
    return False


def bbox_to_tiles(
    bbox: BBox, zoom: int, client_id: Optional[str] = ""
) -> Generator[Tile, None, None]:
    """Convert a bbox to a series of tiles"""
    tile_lower_left: Tile = Tile.lat_lon_to_tile(bbox.minLat, bbox.minLon, zoom)
    tile_upper_right: Tile = Tile.lat_lon_to_tile(bbox.maxLat, bbox.maxLon, zoom)
    if (
        tile_lower_left.x_coordinate > tile_upper_right.x_coordinate
        or tile_lower_left.y_coordinate < tile_upper_right.y_coordinate
    ):
        raise ValueError(f"Lower left isn't lower left: {bbox}")
    y_coordinate: int = tile_lower_left.y_coordinate
    x_coordinate: int = tile_lower_left.x_coordinate
    while y_coordinate >= tile_upper_right.y_coordinate:
        while x_coordinate <= tile_upper_right.x_coordinate:
            yield Tile(x_coordinate, y_coordinate, zoom, client_id=client_id)
            x_coordinate += 1
        y_coordinate -= 1
        x_coordinate = tile_lower_left.x_coordinate


def deduplicate_data(geometry):
    """ Merge all valid LineString data with same key into one contiguous MultiLineString/LineString """
    for feature in geometry:
        current_key = feature["properties"]["key"]
        dup_features = [
            f
            for f in geometry
            if f["properties"]["key"] == current_key and f != feature
        ]
        if len(dup_features) > 0:
            dup_shapes = [shape(f["geometry"]) for f in dup_features]
            dup_shapes.append(shape(feature["geometry"]))
            geom = linemerge(dup_shapes).simplify(0.00005)
            if geom.type == "LineString":
                feature["geometry"]["coordinates"] = [[g[0], g[1]] for g in geom.coords]
            elif geom.type == "MultiLineString":
                coordinates = []
                for line in geom.geoms:
                    coordinates.append([[g[0], g[1]] for g in line.coords])
                feature["geometry"]["type"] = "MultiLineString"
                feature["geometry"]["coordinates"] = coordinates
            for f in dup_features:
                geometry.remove(f)
    return geometry


def compute_tasks(geom):
    """ Generate tasks for a project based off of geometry """
    MAPILLARY_TIME_CORRELATION = current_app.config["MAPILLARY_TIME_CORRELATION"]
    project_tasks = []
    for feature in geom:
        t1 = datetime.datetime.fromtimestamp(
            feature["properties"]["captured_at"] / 1000.0
        )
        task = []
        task.append(feature)
        for correlatedFeature in [
            f
            for f in geom
            if (
                abs(
                    (
                        t1
                        - datetime.datetime.fromtimestamp(
                            (f["properties"]["captured_at"] / 1000.0)
                        )
                    ).total_seconds()
                )
                <= MAPILLARY_TIME_CORRELATION
                and f["properties"]["key"] != feature["properties"]["key"]
            )
        ]:
            # TODO possibly find any additional geometry that is within the
            # MAPILLARY_TIME_CORRELATION for the correlatedFeature itself
            task.append(correlatedFeature)
            geom.remove(correlatedFeature)

        lines = []
        for t in task:
            feat_type = t["geometry"]["type"]
            if feat_type == "LineString":
                lines.append(shape(t["geometry"]))
            elif feat_type == "MultiLineString":
                for c in t["geometry"]["coordinates"]:
                    lines.append(LineString(c))

        shapely_geom = linemerge(lines).simplify(0.00005)
        project_tasks.append(
            geojson.Feature(
                geometry=mapping(shapely_geom),
                properties={
                    "mapillary": [t["properties"]["key"] for t in task],
                    "x": task[0]["properties"]["x"],
                    "y": task[0]["properties"]["y"],
                    "zoom": task[0]["properties"]["zoom"],
                    "isSquare": False,
                },
            )
        )
    return project_tasks


def fetch_additional_tiles(project_tiles, client_id):
    project_tile_coordinates = [p_t.get_tile_coordinates() for p_t in project_tiles]
    additional_tiles = set()
    for t in project_tiles:
        if t.download_additional:
            if Bounds.SOUTH in t.download_additional:
                additional_tiles.add(
                    (
                        Tile(
                            t.x_coordinate,
                            t.y_coordinate + 1,
                            t.zoom,
                            client_id=client_id,
                        ),
                        tuple([k[0] for k in t.cutoff_keys if k[1] == Bounds.SOUTH]),
                    )
                )
            if Bounds.NORTH in t.download_additional:
                additional_tiles.add(
                    (
                        Tile(
                            t.x_coordinate,
                            t.y_coordinate - 1,
                            t.zoom,
                            client_id=client_id,
                        ),
                        tuple([k[0] for k in t.cutoff_keys if k[1] == Bounds.NORTH]),
                    )
                )
            if Bounds.WEST in t.download_additional:
                additional_tiles.add(
                    (
                        Tile(
                            t.x_coordinate - 1,
                            t.y_coordinate,
                            t.zoom,
                            client_id=client_id,
                        ),
                        tuple([k[0] for k in t.cutoff_keys if k[1] == Bounds.WEST]),
                    )
                )
            if Bounds.EAST in t.download_additional:
                additional_tiles.add(
                    (
                        Tile(
                            t.x_coordinate + 1,
                            t.y_coordinate,
                            t.zoom,
                            client_id=client_id,
                        ),
                        tuple([k[0] for k in t.cutoff_keys if k[1] == Bounds.EAST]),
                    )
                )

    for tile, keys in additional_tiles:
        r = requests.get(tile.url)
        if r and r.status_code == 200:
            decoded_data = mapbox_vector_tile.decode(r.content)
            if "mapillary-sequences" in decoded_data:
                decoded_seq = decoded_data["mapillary-sequences"]
                tile.set_extent(int(decoded_seq["extent"]))
                tile_geom = []
                for f in decoded_seq["features"]:
                    if f["properties"]["key"] in keys:
                        tile_geom.append(f)
                if (
                    len(tile_geom) > 0
                    and tile.get_tile_coordinates() not in project_tile_coordinates
                ):
                    tile.set_geometry(tile_geom)
            if tile.get_tile_coordinates() not in project_tile_coordinates:
                project_tiles.append(tile)

    for tile, keys in additional_tiles:
        tile.compute_cutoff_keys(project_tile_coordinates)

    if any(tile.download_additional for tile, keys in additional_tiles):
        fetch_additional_tiles(project_tiles, client_id)


class MapillaryService:
    @staticmethod
    def getMapillarySequences(parameters: dict):
        MAPILLARY_API = current_app.config["MAPILLARY_API"]
        if "bbox" not in parameters:
            raise ValueError("parameters must include a bbox")
        if "start_time" not in parameters and "end_time" not in parameters:
            raise ValueError("parameters must have at least a start or an end time")

        start_epoch, end_epoch = None, None
        # Kaart org key
        org_keys = ["O0K377md1CVrVkzGu4PV5z"]

        if "organization_key" in parameters:
            org_keys.extend(parameters["organization_key"].split(","))

        if "start_time" in parameters:
            start_epoch = datetime.datetime.strptime(
                parameters["start_time"], "%Y-%m-%dT%H:%M:%S.%fZ"
            ).timestamp()
        if "end_time" in parameters:
            end_epoch = datetime.datetime.strptime(
                parameters["end_time"], "%Y-%m-%dT%H:%M:%S.%fZ"
            ).timestamp()

        # request has format: minLon, minLat, maxLon, maxLat
        bbox_list = [float(p) for p in parameters["bbox"].split(",")]
        # BBox expects minLat, minLon, maxLat, maxLon
        bbox = BBox(bbox_list[1], bbox_list[0], bbox_list[3], bbox_list[2])
        z = 14
        initial_tiles = bbox_to_tiles(bbox, z, MAPILLARY_API["clientId"])

        # TODO async this?
        project_tiles = []
        for tile in initial_tiles:
            r = requests.get(tile.url)
            if r and r.status_code == 200:
                decoded_data = mapbox_vector_tile.decode(r.content)
                if "mapillary-sequences" in decoded_data:
                    decoded_seq = decoded_data["mapillary-sequences"]
                    tile.set_extent(int(decoded_seq["extent"]))
                    tile_geom = []
                    for f in decoded_seq["features"]:
                        feature_epoch = f["properties"]["captured_at"] / 1000.0
                        if (
                            (
                                start_epoch
                                and end_epoch
                                and (
                                    feature_epoch >= start_epoch
                                    and feature_epoch <= end_epoch
                                )
                            )
                            or (
                                start_epoch
                                and not end_epoch
                                and feature_epoch >= start_epoch
                            )
                            or (
                                end_epoch
                                and not start_epoch
                                and feature_epoch <= end_epoch
                            )
                        ):
                            if (
                                "organization_key" in f["properties"]
                                and f["properties"]["organization_key"] in org_keys
                                and geom_in_bounds(f, tile, bbox)
                            ):
                                tile_geom.append(f)
                    if len(tile_geom) > 0:
                        tile.set_geometry(tile_geom)
                project_tiles.append(tile)

        if len(project_tiles) == 0:
            raise NotFound()

        for tile in project_tiles:
            tile.compute_cutoff_keys(
                [tile.get_tile_coordinates() for tile in project_tiles]
            )

        if any(tile.download_additional for tile in project_tiles):
            fetch_additional_tiles(project_tiles, MAPILLARY_API["clientId"])

        project_geom = []
        for t in project_tiles:
            if t.geometry and len(t.geometry) > 0:
                project_geom.extend(t.geometry)

        deduplicated_geom = deduplicate_data(project_geom)
        project_tasks = compute_tasks(deduplicated_geom)
        return geojson.FeatureCollection(project_tasks)

    @staticmethod
    def getSequencesAsGPX(project_id: int, task_ids_str: str):
        MAPILLARY_API = current_app.config["MAPILLARY_API"]
        url = (
            MAPILLARY_API["base"]
            + "sequences/{}?client_id="
            + MAPILLARY_API["clientId"]
        )
        headers = {"Accept": "application/gpx+xml"}

        timestamp = datetime.datetime.utcnow()

        XMLNS_NAMESPACE = "http://www.topografix.com/GPX/1/1"
        XMLNS = "{%s}" % XMLNS_NAMESPACE
        XSI_NAMESPACE = "http://www.w3.org/2001/XMLSchema-instance"
        XSI = "{%s}" % XSI_NAMESPACE

        root = ET.Element(
            "gpx",
            attrib=dict(
                xmlns=XMLNS_NAMESPACE,
                version="1.1",
                creator="Kaart Tasking Manager",
            ),
        )
        root.set(XMLNS + "xsi", XSI_NAMESPACE)
        root.set(
            XSI + "schemaLocation",
            "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx .xsd",
        )

        # Create GPX Metadata element
        metadata = ET.Element("metadata")
        link = ET.SubElement(
            metadata,
            "link",
            attrib=dict(href="https://github.com/kaartgroup/tasking-manager"),
        )
        ET.SubElement(link, "text").text = "Kaart Tasking Manager"
        ET.SubElement(metadata, "time").text = timestamp.isoformat()
        root.append(metadata)

        # Create trk element
        trk = ET.Element("trk")
        root.append(trk)
        ET.SubElement(
            trk, "name"
        ).text = f"Task for project {project_id}. Do not edit outside of this area!"

        # Create trkseg element
        trkseg = ET.Element("trkseg")
        trk.append(trkseg)

        if task_ids_str is not None:
            task_ids = map(int, task_ids_str.split(","))
            tasks = Task.get_tasks(project_id, task_ids)
            if not tasks or tasks.count() == 0:
                raise NotFound()
        else:
            tasks = Task.get_all_tasks(project_id)
            if not tasks or len(tasks) == 0:
                raise NotFound()

        for task in tasks:
            key = json.loads(task.extra_properties)["mapillary"]["sequence_key"]
            gpx = requests.get(url.format(key), headers=headers).content
            root2 = ET.fromstring(gpx)
            for trkpt in root2.iter("{http://www.topografix.com/GPX/1/1}trkpt"):
                ET.SubElement(trkseg, "trkpt", attrib=trkpt.attrib)

        sequences_gpx = ET.tostring(root, encoding="utf8")
        return sequences_gpx
