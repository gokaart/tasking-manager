import React, { useEffect, useLayoutEffect, useState, useCallback } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { styleClasses } from '../../views/projectEdit';
import area from '@turf/area';
import transformScale from '@turf/transform-scale';
import bbox from '@turf/bbox';
import buffer from '@turf/buffer';
import { featureCollection } from '@turf/helpers';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { splitTaskGrid, makeGrid } from '../../utils/taskGrid';
import { CustomButton, Button } from '../button';
import { API_URL } from '../../config/index.js';
import {
  UndoIcon,
  MappedIcon,
  CircleIcon,
  FourCellsGridIcon,
  NineCellsGridIcon,
} from '../svgIcons';

export default function SetTaskSizes({ metadata, mapObj, updateMetadata }) {
  const [splitMode, setSplitMode] = useState(null);
  const [mapillarySequencesFound, setMapillarySequencesFound] = useState(null);
  const [fetching, setFetching] = useState(false);

  const splitHandler = useCallback(
    (event) => {
      const taskGrid = mapObj.map.getSource('grid')._data;

      if (metadata.tempTaskGrid === null) {
        updateMetadata({ ...metadata, tempTaskGrid: taskGrid });
      }
      // Make the geom smaller to avoid borders.
      const geom = transformScale(event.features[0].geometry, 0.5);
      const newTaskGrid = splitTaskGrid(taskGrid, geom);

      updateMetadata({
        ...metadata,
        taskGrid: featureCollection(newTaskGrid),
        tasksNumber: featureCollection(newTaskGrid).features.length,
      });
    },
    [updateMetadata, metadata, mapObj.map],
  );

  useEffect(() => {
    if (splitMode === 'click') {
      mapObj.map.on('mouseenter', 'grid', (event) => {
        mapObj.map.getCanvas().style.cursor = 'pointer';
      });
      mapObj.map.on('mouseleave', 'grid', (event) => {
        mapObj.map.getCanvas().style.cursor = '';
      });
      mapObj.map.on('click', 'grid', splitHandler);
    } else {
      mapObj.map.on('mouseenter', 'grid', (event) => {
        mapObj.map.getCanvas().style.cursor = '';
      });
      mapObj.map.off('click', 'grid', splitHandler);
    }
  }, [mapObj, splitHandler, splitMode]);

  const splitDrawing = () => {
    setSplitMode('draw');
    mapObj.map.on('mouseenter', 'grid', (event) => {
      mapObj.map.getCanvas().style.cursor = 'crosshair';
    });
    mapObj.map.on('mouseleave', 'grid', (event) => {
      mapObj.map.getCanvas().style.cursor = '';
    });
    mapObj.map.once('draw.create', (event) => {
      const taskGrid = mapObj.map.getSource('grid')._data;
      if (metadata.tempTaskGrid === null) {
        updateMetadata({ ...metadata, tempTaskGrid: taskGrid });
      }

      const id = event.features[0].id;
      mapObj.draw.delete(id);

      const geom = event.features[0].geometry;
      const newTaskGrid = splitTaskGrid(taskGrid, geom);

      updateMetadata({
        ...metadata,
        taskGrid: featureCollection(newTaskGrid),
        tasksNumber: featureCollection(newTaskGrid).features.length,
      });
      setSplitMode(null);
    });

    mapObj.draw.changeMode('draw_polygon');
  };

  const resetGrid = () => {
    updateMetadata({ ...metadata, taskGrid: metadata.tempTaskGrid });
  };

  const smallerSize = useCallback(() => {
    const zoomLevel = metadata.zoomLevel + 1;
    const squareGrid = makeGrid(metadata.geom, zoomLevel);
    updateMetadata({
      ...metadata,
      zoomLevel: zoomLevel,
      tempTaskGrid: squareGrid,
      taskGrid: squareGrid,
      tasksNumber: squareGrid.features.length,
    });
  }, [metadata, updateMetadata]);

  const grid = useCallback(() => {
    updateMetadata({
      ...metadata,
      taskType: 'grid',
    });
  }, [metadata, updateMetadata]);

  const imagery = useCallback(() => {
    updateMetadata({
      ...metadata,
      taskType: 'imagery',
    });
  }, [metadata, updateMetadata]);

  const mapillary = useCallback(() => {
    updateMetadata({
      ...metadata,
      taskType: 'imagery',
      imageryType: 'mapillary',
    });
  }, [metadata, updateMetadata]);

  const getImageryTasks = useCallback(() => {
    if (metadata.taskGrid) {
      resetGrid();
    }
    if (!mapillarySequencesFound) {
      setMapillarySequencesFound(null);
    }
    // api/v2/system/mapillary-tasks/?bbox=-108.77672575970271,38.837590674185975,-108.23847147765501,39.257936906249626&start_time=2021-05-03T06:00:00.000Z&end_time=2021-05-14T06:00:00.000Z&organization_key=O0K377md1CVrVkzGu4PV5z
    const start_time = metadata.imageStart;
    const end_time = metadata.imageEnd;
    const filters = metadata.imageFilters;
    // Convert geom to bbox
    var api_call = ['bbox=' + bbox(metadata.geom)];
    if (start_time) {
      api_call.push('start_time=' + start_time.toISOString());
    }
    if (end_time) {
      api_call.push('end_time=' + end_time.toISOString());
    }
    if (filters) {
      var tFilters = filters.split(',');
      for (var tempFilter in tFilters) {
        api_call.push(tempFilter);
      }
    }
    // TODO modify if sources other than Mapillary are used
    var imageRetrievalUrl = API_URL + 'system/mapillary-tasks/?' + api_call.join('&');
    setFetching(true);
    fetch(imageRetrievalUrl)
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          if (response.status) {
            if (response.status === 404) {
              setMapillarySequencesFound(false);
            }
            setFetching(false);
            throw response;
          }
        }
      })
      .catch((error) => console.log(error))
      .then((newTasks) => {
        if (newTasks && newTasks.features?.length > 0) {
          let bufferedGeom = buffer(newTasks, 20, { units: 'meters' });
          let features = [];
          for (var f in bufferedGeom.features) {
            if (bufferedGeom.features[f].geometry.type === 'Polygon') {
              bufferedGeom.features[f].geometry.type = 'MultiPolygon';
              // modify coordinates to fit MultiPolygon specifications
              bufferedGeom.features[f].geometry.coordinates = [
                bufferedGeom.features[f].geometry.coordinates,
              ];
            }
            features.push(bufferedGeom.features[f]);
          }
          updateMetadata({
            ...metadata,
            taskGrid: featureCollection(features),
            tasksNumber: features.length,
          });
          setSplitMode(null);
          setMapillarySequencesFound(true);
        } else {
          setMapillarySequencesFound(false);
        }
        setFetching(false);
      });
    // eslint-disable-next-line
  }, [metadata, updateMetadata]);

  const largerSize = useCallback(() => {
    const zoomLevel = metadata.zoomLevel - 1;
    const squareGrid = makeGrid(metadata.geom, zoomLevel);
    if (zoomLevel > 0) {
      updateMetadata({
        ...metadata,
        zoomLevel: zoomLevel,
        tempTaskGrid: squareGrid,
        taskGrid: squareGrid,
        tasksNumber: squareGrid.features.length,
      });
    }
  }, [metadata, updateMetadata]);

  useLayoutEffect(() => {
    if (mapObj.map.getSource('grid') !== undefined) {
      mapObj.map.getSource('grid').setData(metadata.taskGrid);
    } else {
      mapObj.map.addSource('grid', {
        type: 'geojson',
        data: { type: 'FeatureCollection', features: metadata.taskGrid },
      });
    }
    return () => {
      // remove the split on click function when leaving the page
      mapObj.map.off('click', 'grid', splitHandler);
    };
  }, [metadata, mapObj, smallerSize, largerSize, splitHandler]);

  return (
    <>
      <h3 className="f3 ttu fw6 mt2 mb3 barlow-condensed blue-dark">
        <FormattedMessage {...messages.step2} />
      </h3>
      <div>
        <div>
          <p>
            <CustomButton
              className={`bg-white ba ph3 pv2 mr2 ${
                metadata.taskType === 'grid' ? 'red b--red' : 'blue-dark b--grey-light'
              }`}
              onClick={grid}
              icon={<FourCellsGridIcon className="h1 w1 v-mid" />}
            >
              <FormattedMessage {...messages.grid} />
            </CustomButton>
            <CustomButton
              className={`bg-white ba ph3 pv2 mr2 ${
                metadata.taskType === 'imagery' ? 'red b--red' : 'blue-dark b--grey-light'
              }`}
              onClick={imagery}
            >
              <FormattedMessage {...messages.imagery} />
            </CustomButton>
            <br />
            {metadata.taskType === 'grid' && <FormattedMessage {...messages.taskSizes} />}
            {metadata.taskType === 'imagery' && <FormattedMessage {...messages.imagerySource} />}
          </p>
          {metadata.taskType === 'grid' && (
            <div role="group">
              <CustomButton
                className="bg-white blue-dark ba b--grey-light ph3 pv2 mr2"
                onClick={smallerSize}
                icon={<NineCellsGridIcon className="h1 w1 v-mid" />}
              >
                <FormattedMessage {...messages.smaller} />
              </CustomButton>
              <CustomButton
                className="bg-white blue-dark ba b--grey-light ph3 pv2"
                onClick={largerSize}
                icon={<FourCellsGridIcon className="h1 w1 v-mid" />}
              >
                <FormattedMessage {...messages.larger} />
              </CustomButton>
            </div>
          )}
          {metadata.taskType === 'imagery' && (
            <div>
              <CustomButton
                className={`bg-white ba ph3 pv2 mr2 ${
                  metadata.imageryType === 'mapillary' ? 'red b--red' : 'blue-dark b--grey-light'
                }`}
                onClick={mapillary}
              >
                <FormattedMessage {...messages.mapillary} />
              </CustomButton>
              <br />
              {metadata.imageryType !== undefined && (
                <div>
                  <div style={{ display: 'flex' }}>
                    <div className={styleClasses.divClass}>
                      <label className={styleClasses.labelClass} style={{ marginTop: '1em' }}>
                        <FormattedMessage {...messages.imageStart} />
                      </label>
                      <DatePicker
                        selected={metadata.imageStart}
                        name="imageStart"
                        onChange={(startDate) =>
                          updateMetadata({
                            ...metadata,
                            imageStart: startDate,
                          })
                        }
                        // Mapillary was founded Sept. 2013
                        minDate={Date.parse('09/01/2013')}
                        maxDate={metadata.imageEnd ? metadata.imageEnd : Date.now()}
                        dateFormat="MM/dd/yyyy"
                        showYearDropdown
                        scrollableYearDropdown
                        className={styleClasses.inputClass}
                      />
                    </div>
                    <div className={styleClasses.divClass} style={{ paddingBottom: 0 }}>
                      <label className={styleClasses.labelClass} style={{ marginTop: '1em' }}>
                        <FormattedMessage {...messages.imageEnd} />
                      </label>
                      <DatePicker
                        selected={metadata.imageEnd}
                        name="imageEnd"
                        onChange={(endDate) => {
                          updateMetadata({
                            ...metadata,
                            imageEnd: endDate,
                          });
                        }}
                        minDate={
                          metadata.imageStart ? metadata.imageStart : Date.parse('09/01/2013')
                        }
                        maxDate={Date.now()}
                        dateFormat="MM/dd/yyyy"
                        showYearDropdown
                        scrollableYearDropdown
                        className={styleClasses.inputClass}
                      />
                    </div>
                  </div>
                  <div className={styleClasses.divClass}>
                    <label className={styleClasses.labelClass}>
                      <FormattedMessage {...messages.imageFilter} />
                    </label>
                    <input
                      name="imageFilters"
                      type="text"
                      onChange={(e) => {
                        updateMetadata({
                          ...metadata,
                          imageFilters: e.target.value,
                        });
                      }}
                      className={styleClasses.inputClass}
                    />
                  </div>
                  <Button
                    className="white bg-red"
                    disabled={!metadata.imageStart && !metadata.imageEnd}
                    loading={fetching}
                    onClick={getImageryTasks}
                  >
                    <FormattedMessage {...messages.imageTracks} />
                  </Button>
                  <div className="cf pt2">
                    {fetching && <FormattedMessage {...messages.imageFetch} />}
                    {mapillarySequencesFound === false && (
                      <FormattedMessage {...messages.imageNotFound} />
                    )}
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
        <div className="pt3 pb1">
          <p>
            <FormattedMessage {...messages.splitTaskDescription} />
          </p>
          <div role="group">
            <CustomButton
              className={`bg-white ph3 pv2 mr2 ba ${
                splitMode === 'click' ? 'red b--red' : 'blue-dark b--grey-light'
              }`}
              onClick={() => setSplitMode(splitMode === 'click' ? null : 'click')}
              icon={<CircleIcon className="v-mid" style={{ width: '0.5rem' }} />}
            >
              <FormattedMessage {...messages.splitByClicking} />
            </CustomButton>
            <CustomButton
              className={`bg-white ph3 pv2 mr2 ba ${
                splitMode === 'draw' ? 'red b--red' : 'blue-dark b--grey-light'
              }`}
              onClick={splitDrawing}
              icon={<MappedIcon className="h1 w1 v-mid" />}
            >
              <FormattedMessage {...messages.splitByDrawing} />
            </CustomButton>
            <CustomButton
              className="bg-white blue-dark ba b--grey-light ph3 pv2"
              onClick={resetGrid}
              icon={<UndoIcon className="w1 h1 v-mid" />}
            >
              <FormattedMessage {...messages.reset} />
            </CustomButton>
          </div>
        </div>
        <p className="f6 blue-grey lh-title mt3 mb2">
          <FormattedMessage
            {...messages.taskNumberMessage}
            values={{ n: <strong>{metadata.tasksNumber || 0}</strong> }}
          />
        </p>
        <p className="f6 blue-grey lh-title mt1">
          {metadata.taskGrid && metadata.taskGrid.features && (
            <FormattedMessage
              {...messages.taskAreaMessage}
              values={{
                area: (
                  <strong>{(area(metadata.taskGrid.features[0]) / 1e6).toFixed(2) || 0}</strong>
                ),
                sq: <sup>2</sup>,
              }}
            />
          )}
        </p>
      </div>
    </>
  );
}
